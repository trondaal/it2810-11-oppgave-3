const SITE_BASE = "http://127.0.0.1:1337";
export const API_BASE = SITE_BASE + "/api/v1";
export const AUTH_BASE = SITE_BASE + "/auth";

export const AUTH_STATUS = AUTH_BASE + "/loginStatus"
export const AUTH_GOOGLE = AUTH_BASE + "/google";

export const NO_FILTER_ID = -1;
export const API_BEERS = "/beers";
export const API_STYLES = "/styles";
export const API_BREWERIES = "/breweries";
export const API_FAVOURITES = "/user/favourites";
export const DOWN_ARROW = "keyboard_arrow_down";
export const UP_ARROW = "keyboard_arrow_up";