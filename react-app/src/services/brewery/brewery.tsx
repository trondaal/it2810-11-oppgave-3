export class Brewery{
  constructor(
    private _id: number,
    private _name: string,
    private _city: string,
    private _country: string,
    private _description: string,
    private _long: number,
    private _lat: number
  ){}


  get id(){
    return this._id;
  }
  get name(){
    return this._name;
  }
  get city(){
    return this._city;
  }
  get description(){
    return this._description;
  }
  get coords(): [number, number]{
    return [this._lat,this._long];
  }
}