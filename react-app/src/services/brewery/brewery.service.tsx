import { API_BASE, API_BREWERIES, NO_FILTER_ID } from './../../common/constants';

import { Brewery } from './brewery';

import { Subject, Observable } from 'rxjs';

interface searchResult{
    json(): Promise<any>;
}


export interface IBreweryService {
    get_breweries(): Observable<Brewery[]>;
    get_brewery(id: number): Observable<Brewery>
}

export class BreweryService implements IBreweryService{
    /**
     * Get a Promise with an object containing (brewery_name: brewery_id) -pairs
     * @returns {Promise<Object>}
     */
    private _cache = {};
    private got_data = false;
    private b_subjects = [];
    get_brewery(id: number): Observable<Brewery>{
        if(this.got_data)
            return Observable.of(this._cache[id]);
        
        let b = new Subject<Brewery>();
        this.b_subjects.push({
            subject: b,
            id: id
        });
        return b.asObservable();
    }
    get_breweries(): Observable<Brewery[]>{
        let brewery$: Observable<Brewery[]> = 
            Observable.fromPromise<searchResult>(window["fetch"](`${API_BASE}${API_BREWERIES}`,{method:'get'}))
        //Reterive json output from the response (using flatMap because response.json is async
            .flatMap((r,i) => {
                return r.json();
            })
            .map(results => {
                let breweries = [];
                for(let b of results.data){
                    let brewery = new Brewery(b.brewery_id,
                                               b.brewery_name,
                                               b.ciry,
                                               b.country,
                                               b.description,
                                               parseFloat(b.longitude),
                                               parseFloat(b.latitude));
                    this._cache[b.brewery_id] = brewery;
                    breweries.push(brewery);
                }
                this.got_data = true;
                for(let b_s of this.b_subjects){
                    b_s.subject.next(this._cache[b_s.id]);
                    b_s.subject.complete();
                }
                this.b_subjects = [];
                return breweries;
            });
        return brewery$;
        /*
        return new Promise( (resolve, reject) => {
            let breweryObject = {};
            let url = `${API_BASE}${API_BREWERIES}`;
            this.getJsonAsync(url)
                .then( (result) => {
                    //Add "No Filter" option to breweryObject
                    breweryObject["No filter"] = NO_FILTER_ID;
                    //Add breweries to breweryObject
                    for (var i = 0; i < result.data.length; i++){
                        breweryObject[result.data[i].brewery_name] = result.data[i].brewery_id;
                    }
                    resolve(breweryObject);
                })
                .catch(reason => {
                    console.log("Error getting list of breweries. Error message:" + reason);
                    reject(breweryObject);
                });
        })
        */
    }

    /**
     * Get a Promise with an object containing brewery_name: [latitude, longitude] -properties
     * @returns {Promise<Object>}
     */
    /*
    get_breweries_location(){
        return new Promise( (resolve, reject) => {
            let breweryObject = {};
            let url = `${API_BASE}${API_BREWERIES}`;
            this.getJsonAsync(url)
                .then( (result) => {
                    //Add breweries to breweryObject
                    for (var i = 0; i < result.data.length; i++){
                        breweryObject[result.data[i].brewery_name] = [result.data[i].latitude, result.data[i].longitude];
                    }
                    resolve(breweryObject);
                })
                .catch(reason => {
                    console.log("Error getting list of breweries. Error message:" + reason);
                    reject(breweryObject);
                });
        })
    }

    /**
     * Returns a Promise containing json-response from webserver if everything went fine
     * @param url
     * @returns {Observable<R>|Promise<T>|Promise<TResult|T>|any}
     */
    /*
    getJsonAsync(url){
        return window['fetch'](url, {
            method: "get",
            credentials: "include"
        }).then( (result) => {
            return result.json();
        }).catch( (reason) => {
            return reason;
        });
    }
    */
    
}

//Export a single instance of BreweryService
export const breweryService = new BreweryService();
