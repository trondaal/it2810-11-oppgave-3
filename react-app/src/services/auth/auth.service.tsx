import { AUTH_GOOGLE, AUTH_STATUS } from './../../common/constants';

export interface IAuthService {
    performBackendAuthentication();
    checkLoginStatus();
}

class AuthService implements IAuthService {

    performBackendAuthentication() {
        console.log("Performing backend authentication");
        let authUrl = AUTH_GOOGLE;
        return window["fetch"](authUrl, {
            method: 'get',
            credentials: 'include'
        });
    }

    checkLoginStatus() {
        return window["fetch"](AUTH_STATUS, {
            method: 'get',
            credentials: 'include'
        })
        .then(response => response.json())
        .catch(error => console.log(error));
    }
}

export const authService = new AuthService();
