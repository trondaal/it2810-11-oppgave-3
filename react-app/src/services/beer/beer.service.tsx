import { Observable } from 'rxjs';
import { Beer } from './beer';

import { API_BASE, API_BEERS, NO_FILTER_ID, API_FAVOURITES } from './../../common/constants';

import { Brewery, breweryService } from '../brewery';
import { Style, styleService } from '../style';



//A bit hacky
interface searchResult{
    json(): Promise<any>;
    status: number;
}

//Interface for BeerService
export interface IBeerService{
    search_beer(name: string, sort: string, ordercol: string, page: number, pages: number): Observable<Beer[]>;
    getFavouriteBeers(): Observable<Beer[]>;
    removeFavourite(beer: Beer);
}


export class BeerService implements IBeerService{

    /**
     * Performs a search, and returns an Observable
     * @param {string} name
     * @param {string} sort
     * @param {string} ordercol
     * @param {number} page
     * @param {number} pages
     * @param {number} style_id
     * @param {number} brewery_id
     * @return {Observable<Beer[]>} results
     */
    search_beer(name: string, sort: string = 'asc', ordercol: string = 'beer', page: number = 0, pages: number = 1,
                style: Style = new Style(NO_FILTER_ID,""), brewery: Brewery = new Brewery(NO_FILTER_ID,"","","","",0,0)):
    Observable<Beer[]>{
        let url = `${API_BASE}${API_BEERS}?q=${name}&page=${page}&order=${sort}&ordercol=${ordercol}`;
        //if style_id is not id of "No filter" add style=style_id to the url
        style.id != NO_FILTER_ID ? url += `&style=${style.id}` : "";
        //if brewery_id is not id of "No filter" add brewery=brewery_id to the url
        brewery.id != NO_FILTER_ID ? url += `&brewery=${brewery.id}` : "";

        //Create an Observable from a promise, created by fetch
        let beer$: Observable<Beer[]> = Observable.fromPromise<searchResult>(window["fetch"](url,{
            method:'get',
            credentials:'include'
        }))
        .flatMap((r,i) => {
            return r.json();
        })
        .map((r,i) => {
            return r.data;
        })
        .map((beerData,i) => {
            let beers = [];
            for (let b of beerData) {
                beers.push(new Beer(b.beer_id, b.beer_name, b.abv, styleService.get_style(b.style_id), breweryService.get_brewery(b.brewery_id), b.description));
            }
            return beers;
        });
        //Using recursion to retrive more than one page if requested,
        //and combining them together using .zip
        if(pages > 1){
            beer$ = beer$.zip(this.search_beer(name,sort,ordercol,++page,--pages, style, brewery),(a: Beer[],b: Beer[]) => a.concat(b));
        }
        return beer$;
    }

    getFavouriteBeers(): Observable<Beer[]> {
        let url = API_BASE + API_FAVOURITES;

        let beer$: Observable<Beer[]> = Observable.fromPromise<searchResult>(window["fetch"](url,{
            method:'get',
            credentials:'include'
        }))
        //Retrieve json output from the response (using flatMap because response.json is async
            .flatMap((r,i) => {
                if(r.status === 401) {
                    window.location.assign('/');
                };
                return r.json();
            })
            //'unpack' the json response, we only care about the data
            .map((r,i) => {
                console.log(r.data);
                return r.data;
            })
            //Maps beerData to a list of Beer objects
            .map((beerData,i) => {
                let beers = [];
                for (let b of beerData) {
                    console.log(b);
                    beers.push(new Beer(b.beer_id,
                                        b.beer_name,
                                        b.abv,
                                        styleService.get_style(b.style_id),
                                        breweryService.get_brewery(b.brewery_id),
                                        b.description));
                }
                return beers;
            });
        return beer$;
    }

    removeFavourite(beer: Beer) {
        let url = API_BASE + "/user/favourites/" + beer.id;

        return window["fetch"](url,{
            method:'delete',
            credentials:'include'
        })
        .then(response => {
            if(response.status === 401) {
                window.location.assign('/');
            };
            console.log(response.json());
        })
        .catch(error => console.log(error));
    }

    addFavourite(beer: Beer) {
        let url = API_BASE + "/user/favourites/" + beer.id;

        return window["fetch"](url,{
            method:'post',
            credentials:'include'
        })
        .then(response => {
            if(response.status === 401) {
                window.location.assign('/');
            };
            console.log(response.json());
        })
        .catch(error => console.log(error));
    }


}
//Export a single instance of BeerService
export const beerService = new BeerService();
