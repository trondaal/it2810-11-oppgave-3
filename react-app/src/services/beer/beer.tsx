import { Style } from '../style';
import { Brewery } from '../brewery';

import { Observable } from 'rxjs';

export class Beer{
    private _id: number;
    private _name: string;
    private _abv: number;
    private _description: string;
    private _brewery: Brewery;
    private _style: Style;
    constructor(id: number, name: string, abv: number,style: Observable<Style>, brewery: Observable<Brewery>, description: string ){
        this._id = id;
        this._name = name;
        this._abv = abv;
        this._description = description;
        brewery.subscribe(b => this._brewery = b);
        style.subscribe(s => this._style = s);

    }
    get id(): number{
        return this._id;
    }
    get name(): string{
        return this._name;
    }
    get abv(): number{
        return this._abv;
    }
    get breweryName(): string{
        return this._brewery ? this._brewery.name : "";
    }
    get brewery(): Brewery{
        return this._brewery;
    }
    get styleName(): string{
        return this._style ? this._style.name : "";
    }
    get style(): Style{
        return this._style;
    }
    get description(){
        return this._description;
    }

}