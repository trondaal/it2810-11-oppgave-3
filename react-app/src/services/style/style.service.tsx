import { API_BASE, API_STYLES, NO_FILTER_ID } from './../../common/constants';

import { Style } from './style';

import { Subject, Observable } from 'rxjs';

interface searchResult{
    json(): Promise<any>;
}

export interface IStyleService {
    get_styles(): Observable<Style[]>;
    get_style(id: number): Observable<Style>;
}

export class StyleService implements IStyleService{
    /**
     * Get a Promise with an object containing (style_name: style_id) -pairs
     * @returns {Promise<Object>}
     */
    private _cache = {};
    private got_data = false;
    private s_subjects = [];
    get_style(id: number): Observable<Style>{
        if(this.got_data)
            return Observable.of(this._cache[id]);
        
        let s = new Subject<Style>();
        this.s_subjects.push({
            subject: s,
            id: id
        });
        return s.asObservable();
    }
    get_styles(): Observable<Style[]>{
        let style$: Observable<Style[]> = 
            Observable.fromPromise<searchResult>(window["fetch"](`${API_BASE}${API_STYLES}`,{method:'get'}))
        //Reterive json output from the response (using flatMap because response.json is async
            .flatMap((r,i) => {
                return r.json();
            })
            .map(results => {
                let styles = [];
                for(let b of results.data){
                    let style = new Style(b.style_id,b.style_name);
                    this._cache[style.id] = style;
                    styles.push(style);
                }
                this.got_data = true;
                for(let s_s of this.s_subjects){
                    s_s.subject.next(this._cache[s_s.id]);
                    s_s.subject.complete();
                }
                this.s_subjects = [];
                return styles;
            });
        return style$;
        /*return new Promise( (resolve, reject) => {
            let styleObject = {};
            let url = `${API_BASE}${API_STYLES}`;
            this.getJsonAsync(url)
                .then( (result) => {
                    //Add "No Filter" option to styleObject
                    styleObject["No filter"] = NO_FILTER_ID;
                    //Add styles to styleObject
                    for (var i = 0; i < result.data.length; i++){
                        styleObject[result.data[i].style_name] = result.data[i].style_id;
                    }
                    resolve(styleObject);
                })
                .catch(reason => {
                    console.log("Error getting list of styles. Error message:" + reason);
                    reject(styleObject);
                });
        })*/
    }

    /**
     * Returns a Promise containing json-response from webserver if everything went fine
     * @param url
     * @returns {Observable<R>|Promise<T>|Promise<TResult|T>|any}
     */
    getJsonAsync(url){
        return window['fetch'](url, {method: "get"}).then( (result) => {
            return result.json();
        }).catch( (reason) => {
            return reason;
        });
    }
}

//Export a single instance of BreweryService
export const styleService = new StyleService();