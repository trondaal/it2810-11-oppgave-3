import * as React from "react";
import * as ReactDOM from "react-dom";
import { Promise } from 'es6-shim';
import { App } from "./components/app";



ReactDOM.render(
    <App compiler="TypeScript" framework="React" />,
    document.getElementById("root")
);
