import * as React from "react";
import {Subject} from "rxjs";

import { NO_FILTER_ID } from './../../common/constants';


//Component for dropdown selectors one can use to filter search result
export class FilterDropdown extends React.Component<any, any>{
    searchSubject: Subject<{selected : number}> = new Subject<{selected : number}>();
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event){
        this.searchSubject.next({selected: event.target.value});
    }

    componentDidMount(){
        this.searchSubject.distinctUntilChanged()
            .subscribe( (selected) => {
                this.props.onFilterChange(selected);
            })
    }

    render(): JSX.Element {
        let propList = this.props.list;//Object.keys(this.props.object);
        let defval = this.props.default;
        let selected=NO_FILTER_ID;
        let list = [];
        for(let i = 0; i < propList.length; i++){
            if(defval && defval.id == propList[i].id)
                selected = i;
            list.push(
                <option value={i} key={i}>{propList[i].name}</option>
            );
        }
        return (
            <select className="filter" value={selected.toString()} onChange={this.handleChange}>
                <option value={NO_FILTER_ID}>No Filter</option>
                {list}
            </select>
        );
    }
}
