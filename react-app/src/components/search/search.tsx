import * as React from "react";
import { Observable, Subject } from 'rxjs';
import { SearchField, SearchFieldProps } from '../search-field';
import { beerService, Beer } from '../../services/beer';
import { styleService, Style } from '../../services/style';
import { breweryService, Brewery } from "../../services/brewery";
import { Table } from '../list/table';
import { FilterDropdown } from '../filter/filter';
import { DetailView } from '../detail/detailview';
import { NO_FILTER_ID } from './../../common/constants';

export class Search extends React.Component<any,any> {
    term: string = "";
    page: number = 0;
    tableref;
    waitingForPage: boolean  = false;
    styles = [];
    breweries = [];
    constructor(props){
        super(props);
        this.state = {
            selectedBeer: null,
            beers: [],
            sort: "asc",
            ordercol: "beer",
            styleFilter: new Style(NO_FILTER_ID,""),
            breweryFilter: new Brewery(NO_FILTER_ID,"","","","",0,0),
        };
    }

    componentDidMount(){
        //Fetch all styles and breweries
        styleService.get_styles()
            .zip(breweryService.get_breweries(),(a,b) => ({styles:a, breweries: b}))
            .subscribe((v) => {
                this.styles = v.styles;
                this.breweries = v.breweries;
                this.nextSearch("");
                Observable.fromEvent(window.document,'scroll').map((v,i)=>{
                    if(this.tableref){
                        let r = this.tableref.refs.table;
                        let eRect = r.getBoundingClientRect();
                        let bRect = document.body.getBoundingClientRect();
                        let diff = (eRect.top - bRect.top) + r.clientHeight;
                        return diff - (document.body.scrollTop+window.innerHeight);
                    }
                    return 10000;
                }).subscribe((v) => {
                    if(v < 400 && !this.waitingForPage){
                        this.nextPage();
                    }
                });
        });
    }

    /**
     * Perform new search after web app has been loaded
     * @param term
     * @param pages
     */
    nextSearch(term: string,pages=2){
        this.term = term;
        //Every time we perform a new search, reset page counter
        this.page = 1;
        this.waitingForPage = false;
        //Ask for page 0, and page 1
        beerService.search_beer(term, this.state.sort, this.state.ordercol,0,pages, this.state.styleFilter,
            this.state.breweryFilter).subscribe((a)=>{
            //Update our table when we get the search results
            this.updateList(a);
        });
    }

    updateList(beers: Beer[]){
        this.state.beers = beers;
        this.setState(this.state);
    }

    appendList(beers: Beer[]){
        this.state.beers = this.state.beers.concat(beers);
        this.setState(this.state);
    }

    /**
     * Get next page
     * @param pages
     */
    nextPage(pages=1){
        //If we are not already waiting for the next page to arrive
        //or there are no more pages for a given search
        if(!this.waitingForPage){
            //Set waiting flag
            this.waitingForPage = true;
            beerService.search_beer(this.term, this.state.sort, this.state.ordercol, ++this.page,pages,
                this.state.styleFilter, this.state.breweryFilter).subscribe((a)=>{
                //We append to our existing table when we get the next page
                this.appendList(a);
                //No next page if a.length == 0
                this.waitingForPage = a.length == 0;
            });
        }
    }

    setTableRef(ref){
        //Grab the table reference
        this.tableref = ref;
    }

    /**
     * Update this.state and perform new search because state of Table(table) has changed
     * @param state
     */
    handleSortChange(state){
        this.state.beer = this.state.beer;
        this.state.sort = state.sort;
        this.state.ordercol = state.ordercol;

        this.setState(this.state, () => {
            this.nextSearch(this.term);
        });
    }

    /**
     * Update this.state and perform new search because state changed in FilterDropdown (filter) for styles
     * @param state
     */
    handleStyleFilterChange(state) {
        this.state.styleFilter = state.selected >= 0 ? this.styles[state.selected] : new Style(NO_FILTER_ID,"");
        this.setState(this.state, () => {
            this.nextSearch(this.term);
        });
    }

    setSelectedBeer(selected) {
        //Receive selectedBeer from child module, update state to render DetailView
        console.log("selected changed");
        this.state.selectedBeer = selected;
        this.setState(this.state);
        //console.log("search.state.SelectedBeer er nå: " + this.state.selectedBeer.name);
    }

    /**
     * Update this.state and perform new search because state changed in FilterDropdown (filter) for breweries
     * @param state
     */
    handleBreweryFilterChange(state) {
        this.state.breweryFilter = state.selected >= 0 ? this.breweries[state.selected] : new Brewery(NO_FILTER_ID,"","","","",0,0);
        this.setState(this.state, () => {
            this.nextSearch(this.term);
        });
    }
    handleBreweryMapSelect(brewery){
        this.state.breweryFilter = brewery ? brewery : new Brewery(NO_FILTER_ID,"","","","",0,0);
        this.setState(this.state, () => {
            this.nextSearch(this.term);
        });
    }

    render(): JSX.Element {
        console.log("Logged in when rendering search: ", this.props.loggedIn);
        return (
            <div className="flex-container">
                <div className="item details"> <h1>Details</h1>
                    <DetailView listed={this.state.beers} brewery={this.state.breweryFilter} onBreweryChange={(selected) => this.handleBreweryMapSelect(selected)} breweries={this.breweries} select={ this.state.selectedBeer } />
                </div>
                <div className="item searchTable">
                    Filter on style: <FilterDropdown className="filter" list={this.styles} 
                        onFilterChange={ (state) => this.handleStyleFilterChange(state)}
                        default={ this.state.styleFilter }
                    /><br/>
                    Filter on brewery: <FilterDropdown list={this.breweries} 
                        onFilterChange={ (state) => this.handleBreweryFilterChange(state) }
                        default={ this.state.breweryFilter }
                    />
                    <SearchField placeholder="Search on beer name" searchUpdate={(a)=>this.nextSearch(a)} />
                    <Table loggedIn = { this.props.loggedIn }
                           selected = { this.state.selectedBeer }
                           onChange = { (state) => this.handleSortChange(state) }
                           ref = { (ref) => this.setTableRef(ref) }
                           beers = { this.state.beers}
                           changeSelection = {(selected) => this.setSelectedBeer(selected)} />
                    <button onClick={() => this.nextPage()}>Vis mer</button>
                </div>
            </div>
        );
    }
}
