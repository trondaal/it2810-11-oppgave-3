import * as React from "react";
import { Subject } from 'rxjs';

export interface SearchFieldProps { searchUpdate: Function, delay?:number, placeholder?: string }

export class SearchField extends React.Component<SearchFieldProps,any> {
    searchSubject: Subject<string> = new Subject<string>();
    constructor(props){
        super(props);
        this.state = {
        };
    }
    componentDidMount(){

      this.searchSubject
        //Push updates every 300ms or delay specified by prop.delay
        .debounceTime(this.props.delay || 300)
        //Only update if new input has changed
        .distinctUntilChanged()
        .subscribe( (term) => {
          //Callback to props.searchUpdate
          this.props.searchUpdate(term);
        });
    }
    inputChanged(input){
      //Push new value into searchSubject when input changes
      this.searchSubject.next(input.target.value);
    }
    render(): JSX.Element {
        return (
          <input type="text" placeholder={this.props.placeholder} onChange={(input) => {this.inputChanged(input)}}/>
        );
    }
}
