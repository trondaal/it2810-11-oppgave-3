import * as React from "react";
import { Beer } from '../../services/beer/';

export interface TableProps{
    beers: Beer[]
}
export class Table extends React.Component<any,any>{
    constructor(props){
        super(props);
    }

    handleSelection(beer) {
        console.log(beer);
    }

    render(): JSX.Element {


        let rows = [];
        for(let beer of this.props.beers){

            rows.push(
                <tr className="centered" key={beer.id}>
                    <td >{beer.name}</td>
                    <td>{beer.styleName}</td>
                    <td>{beer.breweryName}</td>
                    <td onClick={() => this.props.removeHandler(beer)}>
                        <i className="material-icons fav">star</i>
                    </td>
                </tr>
            );
        }
        return (
            <table ref="table" className="fancy">
            <thead>
                <tr>
                    <th>Beer Name</th>
                    <th>Type</th>
                    <th>Brewery</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>{rows}</tbody>
          </table>
        );
    }
}
