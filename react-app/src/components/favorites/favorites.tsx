import * as React from "react";
import { Beer } from "./../../services/beer";
import { beerService } from "./../../services/beer";
import { Table } from "./table";

export class Favourites extends React.Component<any,any> {
	tableref;
    constructor(props){
        super(props);
        this.state = {
            beers: []
        };
    }

    componentDidMount() {
        beerService.getFavouriteBeers()
        .subscribe(beers => {
            this.updateList(beers);
        });
    }

    updateList(beers: Beer[]){
        this.state.beers = beers;
        this.setState(this.state);
    }

    removeFavourite(beer: Beer) {
        beerService.removeFavourite(beer);
        beerService.getFavouriteBeers()
        .subscribe(beers => {
            this.updateList(beers);
        });
    }

    render(): JSX.Element {
        return (
        	<Table beers={this.state.beers} removeHandler={(beer) => this.removeFavourite(beer)}/>
        );
	}
}
