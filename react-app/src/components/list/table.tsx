import * as React from "react";
import { Beer } from '../../services/beer/';
import { beerService } from '../../services/beer';
import { DOWN_ARROW, UP_ARROW } from '../../common/constants'

export interface TableProps{
    beers: Beer[]
}
export class Table extends React.Component<any,any>{
    beerNameArrowState: string = DOWN_ARROW;
    styleNameArrowState: string = "";
    breweryNameArrowState: string = "";
    constructor(props){
        super(props);
        this.state = {
            sort: "asc",
            ordercol: "beer",
            favouriteBeers: []
        }
    }
    handleClickOnHeader(col : string){
        let newsort = this.state.sort;
        let newcol = this.state.ordercol;

        //set sort state, alternating asc and desc on click on same column
        if (col === this.state.ordercol){
            newsort = newsort === "asc" ? "desc" : "asc";
        } else{
            newcol = col;
            newsort = "asc";
        }
        //set arrow states
        switch (col){
            case "beer":
                this.beerNameArrowState = newsort == "asc" ? DOWN_ARROW : UP_ARROW;
                this.styleNameArrowState = "";
                this.breweryNameArrowState = "";
                break;
            case "style":
                this.beerNameArrowState = "";
                this.styleNameArrowState = newsort == "asc" ? DOWN_ARROW : UP_ARROW;
                this.breweryNameArrowState = "";
                break;
            case "brewery":
                this.beerNameArrowState = "";
                this.styleNameArrowState = "";
                this.breweryNameArrowState = newsort == "asc" ? DOWN_ARROW : UP_ARROW;
        }
        console.log("beerArrow=" + this.beerNameArrowState + " styleArrow=" + this.styleNameArrowState +
            " breweryArrow" + this.breweryNameArrowState);

        this.setState({sort: newsort, ordercol: newcol}, () => {
            this.props.onChange(this.state)
        })
    }

    componentDidMount() {
        if(this.props.loggedIn) {
            this.refreshFavourites();
        }
    }

    refreshFavourites() {
        beerService.getFavouriteBeers().subscribe(beers => this.setState({ favouriteBeers: beers }));
    }

    addFavourite(beer: Beer) {
        console.log("Called add favourite");
        beerService.addFavourite(beer)
        .then(() => this.refreshFavourites());
    }

    removeFavourite(beer: Beer) {
        console.log("Called remove favourite");
        beerService.removeFavourite(beer)
        .then(() => this.refreshFavourites());
    }

    // Call-back to mother to identify selected Beer
    handleSelection(selected: Beer) {
        this.props.changeSelection(selected);
    }

    render(): JSX.Element {
        let favouriteBeerIds = this.state.favouriteBeers.map(beer => beer.id);
        let rows = [];

        for(let i of this.props.beers) {
            let favButton: JSX.Element;
            if(this.props.loggedIn) {
                if(favouriteBeerIds.includes(i.id)) {
                    favButton = (
                        <td onClick={() => this.removeFavourite(i)}>
                            <i className="material-icons fav">star</i>
                        </td>
                    );
                } else {
                    favButton = (
                        <td onClick={() => this.addFavourite(i)}>
                            <i className="material-icons un-fav">star</i>
                        </td>
                    );
                }
            }
            rows.push(

                <tr className={this.props.selected && i.id == this.props.selected.id ? "selected def-cursor" : "def-cursor"} key={i.id}>
                    <td>{i.name}</td>
                    <td>{i.styleName}</td>
                    <td>{i.breweryName}</td>
                    { favButton }
                    <td className="pointer" onClick={() => this.handleSelection(i)}>
                        <a> Details </a>
                    </td>
                </tr>
            );
        }
        return (
            <table ref="table" className="fancy">
            <thead>
                <tr>
                    <th onClick={() => this.handleClickOnHeader("beer")}>
                        Beer Name<i className="material-icons arrows">{this.beerNameArrowState}</i></th>
                    <th onClick={() => this.handleClickOnHeader("style")}>
                        Style<i className="material-icons arrows">{this.styleNameArrowState}</i></th>
                    <th onClick={() => this.handleClickOnHeader("brewery")}>
                        Brewery<i className="material-icons arrows">{this.breweryNameArrowState}</i></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>{rows}</tbody>
          </table>
        );
    }
}
