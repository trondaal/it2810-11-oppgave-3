import * as React from "react";
import { API_BASE } from "./../../common/constants"
import { Favourites } from "./../favorites"

export class Profile extends React.Component<any,any>{
    constructor(props){
        super(props);
        this.state = {
            name: "name",
            email: "email"
        }

        this.getUserInfo = this.getUserInfo.bind(this);
    }

    componentDidMount() {
        this.getUserInfo().then(info => this.setState(info));
    }

    getUserInfo() {
        return window["fetch"](API_BASE + "/user", {
            method: "get",
            credentials: 'include'
        })
        .then(response => {
            if(response.status === 401) {
                window.location.assign('/');
            } else {
                return response.json()
            }
        })
        .then(json => {
            return {
                name: json.data[0].name,
                email: json.data[0].email
            };
        })
        .catch(error => console.log(error));
    }

    render(): JSX.Element {
        return (
            <div>
                <div className="profile">
                    <h3>Info</h3>
                    <ul>
                        <li id="name">Name: {this.state.name} </li>
                        <li id="mail">Email: {this.state.email} </li>
                    </ul>
                </div>
                <h3> Favourite Beers </h3>
                <Favourites />
            </div>
        );
    }
}
