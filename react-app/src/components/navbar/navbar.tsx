import * as React from "react";
import { Search } from '../search';
import { Profile } from '../profile';
import { GoogleSignin, GoogleSignout } from "../auth"

export class NavBar extends React.Component<any, any> {

    constructor(props) {
        super(props);
    }

    render(): JSX.Element {
        let buttons: Array<JSX.Element>;

        if(this.props.loggedIn) {
            buttons = [
                <button onClick={ () => this.props.changeHandler("profile") } className="mySite"  key='0'> My profile </button>,
                <button onClick={ () => this.props.changeHandler("search") } className="beerSite" key='1'> Beers</button>
            ];
        }
        let googleButton: JSX.Element = this.props.loggedIn ? <GoogleSignout/> : <GoogleSignin/> ;

        return (
            <div>
                <nav className="navbar">
                    <h1 className="nav-title">Beer with us!</h1>
                    { googleButton }
                    { buttons }
                </nav>
            </div>
        );
    }
}
