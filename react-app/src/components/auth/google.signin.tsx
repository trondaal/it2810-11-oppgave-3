import * as React from "react";

export class GoogleSignin extends React.Component<any,any> {
    constructor(props){
        super(props);
    }

    render(): JSX.Element {
        return (
            <a href="/auth/google">
                <img src="img/signin_btn.png" height="75%"/>
            </a>
        );
    }
}
