import * as React from "react";

export class GoogleSignout extends React.Component<any,any> {
    constructor(props){
        super(props);
    }

    render(): JSX.Element {
        return (
            <a href="/auth/signout">
                <button> Sign out </button>
            </a>
        );
    }
}
