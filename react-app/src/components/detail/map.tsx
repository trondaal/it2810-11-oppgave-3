import * as React from "react";
import ReactDOM from 'react-dom';
import L from 'leaflet';
declare var require: any

//import { Map, Marker, Popup, TileLayer} from 'react-leaflet';

const{ Map, Marker, Popup, TileLayer, MapLayer } = require('../../../node_modules/react-leaflet');


export class Leaflet extends React.Component<any,any> {
  constructor(props){
    super(props);
  }
  selectBrewery(brewery){
    if(this.props.onBreweryChange){
      this.props.onBreweryChange(brewery);
    }
    //this.forceUpdate();
  }
  render(): JSX.Element {
    const position = [51.505, -0.09];
    const markers = [];
    const breweriesSet = {};
    const listedBeers = this.props.listed;
    console.log(listedBeers,listedBeers ? true : false);
    if(listedBeers){
      for(let i of listedBeers){
        let m = i.brewery;
        if(!breweriesSet[m.id]){
          breweriesSet[m.id] = true;
          markers.push(
            <Marker key={m.id} position={m.coords}>
              <Popup>
                <span className="marker-popup">
                  <h2>{m.name}</h2>
                  <div className="marker-description">{m.description}</div>
                  <span className={"marker-button " + ((m.id == this.props.brewery.id) ? "selected" : "")} onClick={()=>this.selectBrewery(m)}>Vis øl</span>
                </span>
              </Popup>
            </Marker>
            )
        }
      }
    }
    /*if(this.props.breweries)
      for(let i in this.props.breweries){
        let m = this.props.breweries[i];
        markers.push(
          <Marker key={m.id} position={m.coords}>
            <Popup>
              <span className="marker-popup">
                <h2>{m.name}</h2>
                <div className="marker-description">{m.description}</div>
                <span className={"marker-button " + ((m.id == this.props.brewery.id) ? "selected" : "")} onClick={()=>this.selectBrewery(i)}>Vis øl</span>
              </span>
            </Popup>
          </Marker>
        )
      }*/
    const map = (
      <Map id="map1" center={this.props.center.coords} zoom={7}>
        <TileLayer
          url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        {markers}
      </Map>
    );
    return (map)
  }
}
