import * as React from "react";
import { Beer } from '../../services/beer/';
import { Leaflet } from "./map";


export interface DetailProps{
    beers: Beer[]
}
export class DetailView extends React.Component<any,any>{
    constructor(props){
        super(props);
        this.state = {
            beers: [],
        };
    }

    render(): JSX.Element {
        //Check that component has received a Beer
        if (this.props.select != null){
                //console.log("oppdatert DetailView for: " + this.props.select.name);
                // Render component when Beer is received
                return (
                    <div>
                        <h2>Details about {this.props.select.name}</h2>
                        <div>Name: {this.props.select.name}</div>
                        <div>Style: {this.props.select.styleName}</div>
                        <div>Brewery: {this.props.select.breweryName}</div>
                        <div>Alcohol level: {this.props.select.abv}%</div>
        
                        <div>Description:  {this.props.select.description}</div>

                        <div id="map1">
                            <Leaflet listed={this.props.listed} brewery={this.props.brewery} onBreweryChange={this.props.onBreweryChange} breweries={this.props.breweries} center={this.props.select.brewery} />
                        </div>
                    </div>
                );
        
        }
        //Do not show component if no Beer received
        else return (
            <p>Click on Details in a table row to see more information about a beer.</p>
        );
    }
}