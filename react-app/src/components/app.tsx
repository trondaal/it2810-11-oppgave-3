import * as React from "react";
import { Search } from "./search";
import { Profile } from "./profile";
import { NavBar } from "./navbar";
import { authService } from '../services/auth';

export class App extends React.Component<any,any> {
    constructor(props){
        super(props);
        this.state = {
            activeComponent: null,
            userLoggedIn: false
        };
    }
    componentDidMount() {
        authService.checkLoginStatus().then(status => {
            this.setState({ userLoggedIn: status.loggedIn });
            this.setActiveComponent("search");
        });
    }

    setActiveComponent(componentString: string) {
        let component;
        switch(componentString) {
            case "search":
                component = <Search loggedIn={this.state.userLoggedIn} />
                break;
            case "profile":
                component = <Profile loggedIn={this.state.userLoggedIn} />
                break;
            default:
                component = <Search loggedIn={this.state.userLoggedIn} />
                break;
        }
        this.setState({ activeComponent: component });
    }

    render(): JSX.Element {
        return (
            <div>
                <NavBar changeHandler = {component => this.setActiveComponent(component)} loggedIn={this.state.userLoggedIn} />
                <div className="flex-container">
                    <div className="flex flex-col flex-center flex-65">
                        { this.state.activeComponent }
                    </div>
                </div>
            </div>
        );
    }
}
