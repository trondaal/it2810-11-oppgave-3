var express = require("express");
var router = express.Router();
var beerDB = require("./beerDb.js")();
var passport = require('passport');

router.get('/loginStatus', function(req, res) {
    req.isAuthenticated() ? res.send({'loggedIn' : true}) : res.send({'loggedIn' : false});
});

router.get('/signout', function(req, res) {
    if(req.isAuthenticated()) {
        req.logout();
    }
    res.redirect('/');
});

// Redirects the user to the Google sign-in page
router.get('/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

// Receives callback from Google sign-in after user authentication
router.get('/google/callback', passport.authenticate('google', {
        successRedirect : '/',
        failureRedirect : '/'
    })
);

module.exports = router;
