module.exports = function() {

    var connection = null;

    /*
    *   An object containing the various queries we want to perform on the database
    */
    var queries = {

        beersByName: (beerName, breweryId, styleId, order, offset) => {
            return {
                query: 'SELECT beers.id AS beer_id, beers.name AS beer_name, breweries.id AS brewery_id, breweries.name AS brewery_name, ' +
                       'beers.abv AS abv, styles.id AS style_id, styles.name AS style_name, beers.description AS description ' +
                       'FROM beers JOIN breweries ON beers.brewery_id = breweries.id ' +
                       'JOIN styles ON beers.style_id = styles.id ' +
                       'WHERE beers.name RLIKE ? AND breweries.id LIKE ? AND styles.id LIKE ? ' +
                       order +
                       'LIMIT 10 OFFSET ?;',
                parameters: [beerName, breweryId, styleId, offset]
            };
        },

        beerWithId: beerId => {
            return {
                query: 'SELECT beers.id AS beer_id, beers.name AS beer_name, breweries.id AS brewery_id, breweries.name AS brewery_name, ' +
                       'beers.abv AS abv, styles.id AS style_id, styles.name AS style_name, beers.description AS description ' +
                       'FROM beers JOIN breweries ON beers.brewery_id = breweries.id ' +
                       'JOIN styles ON beers.style_id = styles.id ' +
                       'WHERE beers.id = ?;',
                parameters: [beerId]
            };
        },

        allBreweries: (order, offset) => {
            return {
                query: 'SELECT breweries.id AS brewery_id, breweries.name AS brewery_name, ' +
                       'breweries.latitude AS latitude, breweries.longitude AS longitude, ' +
                       'breweries.city AS city, breweries.country AS country, breweries.description AS description ' +
                       'FROM breweries ' +
                        order +
                        (offset == -1 ? ';' : 'LIMIT 10 OFFSET ?;'),
                parameters: [offset]
            };
        },

        breweryWithId: breweryId => {
            return {
                query: 'SELECT breweries.id AS brewery_id, breweries.name AS brewery_name, ' +
                       'breweries.latitude AS latitude, breweries.longitude AS longitude, ' +
                       'breweries.city AS city, breweries.country AS country, breweries.description AS description ' +
                       'FROM breweries ' +
                       'WHERE breweries.id = ?;',
                parameters: [breweryId]
            };
        },

        allStyles: (order, offset) => {
            return {
                query: 'SELECT styles.id AS style_id, styles.name AS style_name ' +
                       'FROM styles ' +
                       order +
                       (offset == -1 ? ';' : 'LIMIT 10 OFFSET ?;'),
                parameters: [offset]
            };
        },

        favouritesForUserWithId: (id, order) => {
            return {
                query: 'SELECT beers.id AS beer_id, beers.name AS beer_name, breweries.id AS brewery_id, breweries.name AS brewery_name, ' +
                       'beers.abv AS abv, styles.id AS style_id, styles.name AS style_name, beers.description AS description ' +
                       'FROM beers JOIN breweries ON beers.brewery_id = breweries.id ' +
                       'JOIN styles ON beers.style_id = styles.id ' +
                       'JOIN user_favourites ON beers.id = user_favourites.beer_id ' +
                       'WHERE user_favourites.user_id = ? ' +
                       order + ';',
                parameters: [id]
            };
        },

        addFavouriteBeer: (userId, beerId) => {
            return {
                query: 'INSERT INTO user_favourites (user_id, beer_id) ' +
                       'VALUES (?, ?);',
                parameters: [userId, beerId]
            };
        },

        removeFavouriteBeer: (userId, beerId) => {
            return {
                query: 'DELETE FROM user_favourites ' +
                       'WHERE user_id = ? AND beer_id = ?;',
                parameters: [userId, beerId]
            };
        },

        // User queries
        userWithId: userId => {
            return {
                query: 'SELECT * ' +
                       'FROM users ' +
                       'WHERE id = ?;',
                parameters: [userId]
            };
        },

        newUser: (googleId, token, name, email) => {
            return {
                query: 'INSERT INTO users (id, token, name, email) ' +
                       'VALUES (?, ?, ?, ?);',
                parameters: [googleId, token, name, email]
            };
        },

        profileInfo: (userId) => {
            return {
                query: 'SELECT name, email ' +
                       'FROM users ' +
                       'WHERE id = ?;',
                parameters: [userId]
            };
        }

    };

    // Sorts all beers by different attributes
    var sortOrders = {
        ascendingBeerId:  () => { return 'ORDER BY beer_id ASC '  },
        descendingBeerId: () => { return 'ORDER BY beer_id DESC ' },
        ascendingBeerName: () => { return 'ORDER BY beer_name ASC ' },
        descendingBeerName: () => { return 'ORDER BY beer_name DESC ' },
        ascendingBreweryId: () => { return 'ORDER BY brewery_id ASC ' },
        descendingBreweryId: () => { return 'ORDER BY brewery_id DESC ' },
        ascendingBreweryName: () => { return 'ORDER BY brewery_name ASC ' },
        descendingBreweryName: () => { return 'ORDER BY brewery_name DESC ' },
        ascendingStyleId: () => { return 'ORDER BY style_id ASC ' },
        descendingStyleId: () => { return 'ORDER BY style_id DESC ' },
        ascendingStyleName: () => { return 'ORDER BY style_name ASC ' },
        descendingStyleName: () => { return 'ORDER BY style_name DESC ' }
    }

    // Performs a query on the database, and returns a promise with the results as JSON.
    var executeQuery = (query) => {
        return new Promise((resolve, reject) => {
            connection.query(query.query, query.parameters, (error, results, fields) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(JSON.parse(JSON.stringify(results)));
                };
            });
        });
    }

    var findUserByGoogleId = (id) => {
        return executeQuery(queries.userWithId(id));
    }

    var createNewUser = (googleId, token, name, email) => {
        return executeQuery(queries.newUser(googleId, token, name, email))
        .then(results => findUserByGoogleId(googleId));
    }

    var findOrCreateUser = (googleId, name, email) => {
        return findUserByGoogleId(googleId)
        .then(user => {
            if(user[0]) {
                return new Promise((resolve, reject) => resolve(user));
            } else {
                return createNewUser(googleId, 'lol, not used', name, email);
            }
        });
    }

    /**
     * Returns all beers whose names contain 'beerName'
     * @param {string}      beerName    - The string that should be contained in the name of the beer. An empty string returns all beers
     * @param {integer}     page        - (OPTIONAL) The offset of the results that we want (defaults to 0)
     * @param {sortOrder}   order       - (OPTIONAL) The order according to which the rows should be sorted (defaults to ascending beer id)
     */
    var getBeersByName = (inputName, inputBreweryId, inputStyleId, page = 0, order = sortOrders.ascendingBeerName()) => {
        let offset = calculateOffsetForPage(page);

        let beerName = (inputName === '' || inputName === undefined) ? '.' : inputName;
        let breweryId = (typeof inputBreweryId !== 'number' || inputBreweryId === undefined) ? '%' : inputBreweryId;
        let styleId = (typeof inputStyleId !== 'number' || inputStyleId === undefined) ? '%' : inputStyleId;

        return executeQuery(queries.beersByName(beerName, breweryId, styleId, order, offset));
    }

    /**
     * Returns the beer with id 'beerId'
     * @param {string}  beerId  - The id of the beer we want returned
     */

    var getBeerWithId = (beerId) => {
        return executeQuery(queries.beerWithId(beerId));
    }

    /**
     * Returns all beers
     * @param {integer}     page        - (OPTIONAL) The offset of the results that we want (defaults to 0)
     * @param {sortOrder}   order       - (OPTIONAL) The order according to which the rows should be sorted (defaults to ascending brewery name)
     */

    var getAllBreweries = (page = 0, order = sortOrders.ascendingBreweryName()) => {
        let offset = calculateOffsetForPage(page);
        return executeQuery(queries.allBreweries(order, offset));
    }

    /**
     * Returns the brewery with id 'breweryId'
     * @param {string}  breweryId  - The id of the brewery we want returned
     */
    var getBreweryWithId = (breweryId) => {
        return executeQuery(queries.breweryWithId(breweryId));
    }

    var getFavouriteBeers = (userId, order = sortOrders.ascendingBeerName()) => {
        return executeQuery(queries.favouritesForUserWithId(userId, order));
    }

    var addFavouriteBeer = (userId, beerId) => {
        return getFavouriteBeers(userId)
        .then(beers => {
            if(! beers.map(beer => Number(beer.beer_id)).includes(Number(beerId))) {
                return executeQuery(queries.addFavouriteBeer(userId, beerId));
            }
            return {message : 'Beer is already a favourite of this user'}
        })
    }

    var removeFavouriteBeer = (userId, beerId) => {
        return getFavouriteBeers(userId)
        .then(beers => {
            if(beers.map(beer => Number(beer.beer_id)).includes(Number(beerId))) {
                return executeQuery(queries.removeFavouriteBeer(userId, beerId));
            }
            return {message : 'Beer is not a favourite of this user'}
        })
    }

    var getUserInfo = (userId) => {
        return executeQuery(queries.profileInfo(userId));
    }

    /**
     * Returns all styles
     * @param {integer}     page        - (OPTIONAL) The offset of the results that we want (defaults to 0)
     * @param {sortOrder}   order       - (OPTIONAL) The order according to which the rows should be sorted (defaults to ascending style id)
     */
    var getAllStyles = (page = 0, order = sortOrders.ascendingStyleName()) => {
        let offset = calculateOffsetForPage(page)
        return executeQuery(queries.allStyles(order, offset))
    }


    var calculateOffsetForPage = (page) => {
        return page === -1 ? page : page * 10;
    }

    //Initialize the database connection
    return (() => {
        let mysql = require('mysql');
        let database_credentials = require("./database_credentials");
        connection = mysql.createConnection({

            host: database_credentials.host,
            user: database_credentials.user,
            password: database_credentials.password,
            database: database_credentials.database,

        });
        connection.connect();

    // Return an object with the functions we want to expose
        return {
            getBeersByName,
            getBeerWithId,
            getAllBreweries,
            getBreweryWithId,
            getAllStyles,
            getFavouriteBeers,
            addFavouriteBeer,
            removeFavouriteBeer,
            findOrCreateUser,
            findUserByGoogleId,
            getUserInfo,
            sortOrders
        };
    })();
}
