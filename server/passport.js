var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var authConfig = require('./auth_config');
var beerDb = require('./beerDb')();

module.exports = function(passport) {

    // Serializes user for sessions
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // Deserializes user for sessions
    passport.deserializeUser(function(id, done) {
        beerDb.findUserByGoogleId(id)
        .then(user => done(null, user[0]))
        .catch(err => done(err));
    });

    // Use the Google strategy
    passport.use(new GoogleStrategy(authConfig.googleAuth, (token, refreshToken, profile, done) => {

        process.nextTick(() => {
            beerDb.findOrCreateUser(profile.id, profile.displayName, profile.emails[0].value)
            .then(users => done(null, users[0]))
            .catch(err => done(err))
        });
    }));
};
