### Hvordan sette opp MySQL med Docker og PhpMyAdmin ###

1. Installér [Docker](https://www.docker.com/)!

2. I Bash/PowerShell skriv:

```bash
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=passord -d mysql:5.7
```
3. Skriv ```docker ps``` og verifiser at en container med navn ```mysql``` er i listen som kommer opp

4. Skriv deretter
```bash
docker run --name myadmin -d --link mysql:db -p 8080:80 phpmyadmin/phpmyadmin
```

5. Skriv ```docker ps``` og verifiser at både containerne ```mysql``` og ```myadmin``` er i listen som kommer opp

6. I nettleseren, gå [hit](http://localhost:8080/phpmyadmin) og logg inn med brukernavn 'root' og passordet du valgte i steg 2.
(Fungerer ikke for meg i Chrome, prøv med en annen nettleser)


7. Opprett en ny database med navnet ```beer_db```

8. I databasen, gå til fanen som heter ```Import```

9. Velg filen ```beer_db.mysql``` som ligger i Google Drive i mappen ```WebApp```

10. Trykk på GO!

11. Alt skal nå fungere.
