var express = require("express");
var router = express.Router();
var beerDB = require("./beerDb.js")();
//prefix is: /api/v1

function isLoggedIn(req, res, next) {
    if(req.isAuthenticated()) {
        return next();
    }
    res.status(401).send();
}


// Sets appropriate headers
router.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", 'http://127.0.0.1:1337');
	res.header("Access-Control-Allow-Credentials", true);
    res.header("Access-Control-Allow-Methods", 'GET, POST, DELETE');
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
})


//The following four methods implements API v1 (with helper functions), see documentations for more information

//ask db about details of selected beer
router.get('/beers/:beer_id', function(req, res) {
    beerDB.getBeerWithId(req.params.beer_id)
        .then(function (results) {
            res.send(addMeta(results));
        })
        .catch(function (error) {
            res.send(addErrorInformation(error));
        })
});

//ask db about all breweries
router.get('/breweries', function(req, res) {
    beerDB.getAllBreweries(-1)
        .then(function (results) {
            res.send(addMeta(results));
        })
        .catch(function (error) {
            res.send(addErrorInformation(error));
    })
});

//ask db about a list of all beer styles
router.get('/styles', function(req, res) {
    beerDB.getAllStyles(-1)
        .then(function (results) {
            res.send(addMeta(results));
        })
        .catch(function (error) {
            res.send(addErrorInformation(error));
    })
});

//ask db about q=...&page=...&order=...&ordercol=...&brewery=...&style=...
router.get('/beers?*', function(req, res) {
    var q = getValidSearch(req.query.q);
    var page = getValidValueForPage(req.query.page);
    var sortValue = getValidValueForSortOrders(req.query.order, req.query.ordercol)
    var brewery_id = req.query.brewery;
    brewery_id = typeof brewery_id === "undefined" ? null : Number(brewery_id);
    var style_id = req.query.style;
    style_id = typeof style_id === "undefined" ? null : Number(style_id);

    var promise = beerDB.getBeersByName(q, brewery_id, style_id, page, sortValue);

    promise
        .then(function (result) {
            res.send(addMeta(result));
        })
        .catch(function (error) {
            res.send(addErrorInformation(error));
        })
});

/*
*   PRIVILEGED ROUTES
*   To use these next four routes the user will have to use an authenticated cookie,
*   which can be obtained by signing in with Google
*/

/*
*   Retrieves details about the user
*/

router.get('/user', isLoggedIn, function(req, res) {
    let userId = req.user.id;

    beerDB.getUserInfo(userId)
    .then(userInfo => res.send(addMeta(userInfo)))
    .catch(error => res.send(addErrorInformation("Could not retrieve user details.")));
});

/*
*   Retrieves this user's favourite beers
*/

router.get('/user/favourites', isLoggedIn, function(req, res) {
    let userId = req.user.id;
    console.log("requested favourites");

    beerDB.getFavouriteBeers(userId)
    .then(favouriteBeers => res.send(addMeta(favouriteBeers)) )
    .catch(error => res.send(addErrorInformation(error)))
});

/*
*   Adds a beer to the user's favourite beers
*/

router.post('/user/favourites/:beerId', isLoggedIn, function(req, res) {
    let userId = req.user.id;
    let beerId = req.params.beerId;

    beerDB.addFavouriteBeer(userId, beerId)
    .then(message => res.send(addMeta(message.message)))
    .catch(error => res.send(addErrorInformation(error)))
})

/*
*   Removes a beer from the user's favourite beers
*/

router.delete('/user/favourites/:beerId', isLoggedIn, function(req, res) {
    let userId = req.user.id;
    let beerId = req.params.beerId;

    beerDB.removeFavouriteBeer(userId, beerId)
    .then(message => res.send(addMeta(message.message)))
    .catch(error => res.send(addErrorInformation(error)))
});



//helper functions

//helper function for get(/beers?*... -function
function getValidSearch(q) {
    if (typeof q === "undefined"){
        return "";
    }
    return q;
}

//helper function for get(/beers?*... -function
function getValidValueForPage(page) {
    if (typeof page === "undefined"){
        return 0;
    }
    return page;
}

//helper function for get(/beers?*... -function
function getValidValueForSortOrders(order, ordercol){
    var sortValue;
    var order;
    if (typeof order === "undefined"){
        order = "asc";
    }
    if (typeof ordercol === "undefined"){
        sortValue =  order=="asc" ? beerDB.sortOrders.ascendingBeerName() : beerDB.sortOrders.descendingBeerName();
    } else {
        if (ordercol=="name"){
            sortValue = order=="asc" ? beerDB.sortOrders.ascendingBeerName() : beerDB.sortOrders.descendingBeerName();
        } else if (ordercol=="style"){
            sortValue = order=="asc" ? beerDB.sortOrders.ascendingStyleName() : beerDB.sortOrders.descendingStyleName();
        } else if (ordercol=="brewery"){
            sortValue = order=="asc" ? beerDB.sortOrders.ascendingBreweryName() : beerDB.sortOrders.descendingBreweryName()
        } else if (ordercol=="beer"){
            sortValue = order=="asc" ? beerDB.sortOrders.ascendingBeerName() : beerDB.sortOrders.descendingBeerName();
        }
    }
    return sortValue;
}

/**
 * Adds information; status and numObjects and returns json object with data-objects from beerDB.js in data property
 * @param  {} dataBaseArray the array with objects returned from beerDB.js
 */
function addMeta(dataBaseArray) {
    var serverDataSet = {
        status: "success",
        numObjects: dataBaseArray.length,
        data: dataBaseArray
    }
    return serverDataSet;
}

/**
 * Adds information; status and message and returns json object
 * @param  {} error message from beerDB.js
 */
function addErrorInformation(error) {
    var serverDataSet = {
        status: "error",
        message: error,
    }
    return serverDataSet;
}

module.exports = router;
