// Server dependencies
var express = require("express");
var session = require("express-session");
var cookieParser = require('cookie-parser');
var passport = require('passport');
var bodyParser = require('body-parser');

var apiVersion1 = "/api/v1";
var authPath = "/auth";

var app = express();
var port = 1337;

require('./passport')(passport);

app.use(cookieParser());

// Middleware for easily parsing request bodies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(session({
    secret: 'mysecret',
    saveUninitialized: true,
    resave: true,
    cookie: {
        // Cookie duration of one hour
        maxAge: 3600000,
        secure: false
    }
}));

app.use(passport.initialize());
app.use(passport.session());

//loads up router with prefix for all routes defined in first argument, and
//routes defined in second, in our case all routes are defined in its own file
app.use(apiVersion1, require("./router_apiv1"));
app.use(authPath, require("./router_auth"));
app.use('/', require("./router_react"));

// serve webpacked bundle and css statically
app.use(express.static('static'));

//start server at defined port
app.listen(port, function () {
    console.log("Listening on port " + port);
});
