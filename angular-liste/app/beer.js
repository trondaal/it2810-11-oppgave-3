"use strict";
var Beer = (function () {
    function Beer(beer_id, beer_name, style_id, style_name, abv, brewery_id, brewery_name, description) {
        this.beer_id = beer_id;
        this.name = beer_name;
        this.style_id = style_id;
        this.style_name = style_name;
        this.abv = abv;
        this.brewery_id = brewery_id;
        this.brewery_name = brewery_name;
        this.description = description;
    }
    return Beer;
}());
exports.Beer = Beer;
//# sourceMappingURL=beer.js.map