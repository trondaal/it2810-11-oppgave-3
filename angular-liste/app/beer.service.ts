import { Injectable } from '@angular/core';
import { Beer } from './beer';
import { Observable} from 'rxjs';
import { Http } from "@angular/http";
import "rxjs/add/operator/map";
const API_BASE = 'http://localhost:1337/api/v1';
const API_BEER = "/beers";


@Injectable()
export class BeerService {
	constructor(private http: Http){}

	search_beer(name: string,page: number = 0,pages: number = 1): Observable<Beer[]>{
        let url = `${API_BASE}${API_BEER}?q=${name}&page=${page}`; // new window.URL(API_BEER + API_BASE);
        let beer$ = this.http.get(url)
            .map<any>((r,i) => {
                return r.json().data;
            })
            .map((beerData,i) => {
				console.log(beerData);
                let beers = [];
                for(let b of beerData){
                    beers.push(new Beer(b.beer_id,b.beer_name, b.style_id, b.style_name, b.abv, b.brewery_id, b.brewery_name, b.description));
                }
                return beers;
            }
        );
        //Combines page 0 and 1
        if(pages > 1){
            beer$ = beer$.zip(this.search_beer(name,++page,--pages),(a,b) => a.concat(b));
        }
        return beer$;
    }
}
