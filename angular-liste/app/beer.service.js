"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var beer_1 = require('./beer');
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var API_BASE = 'http://localhost:1337/api/v1';
var API_BEER = "/beers";
var BeerService = (function () {
    function BeerService(http) {
        this.http = http;
    }
    BeerService.prototype.search_beer = function (name, page, pages) {
        if (page === void 0) { page = 0; }
        if (pages === void 0) { pages = 1; }
        var url = "" + API_BASE + API_BEER + "?q=" + name + "&page=" + page; // new window.URL(API_BEER + API_BASE);
        var beer$ = this.http.get(url)
            .map(function (r, i) {
            return r.json().data;
        })
            .map(function (beerData, i) {
            console.log(beerData);
            var beers = [];
            for (var _i = 0, beerData_1 = beerData; _i < beerData_1.length; _i++) {
                var b = beerData_1[_i];
                beers.push(new beer_1.Beer(b.beer_id, b.beer_name, b.style_id, b.style_name, b.abv, b.brewery_id, b.brewery_name, b.description));
            }
            return beers;
        });
        //Combines page 0 and 1
        if (pages > 1) {
            beer$ = beer$.zip(this.search_beer(name, ++page, --pages), function (a, b) { return a.concat(b); });
        }
        return beer$;
    };
    BeerService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], BeerService);
    return BeerService;
}());
exports.BeerService = BeerService;
//# sourceMappingURL=beer.service.js.map