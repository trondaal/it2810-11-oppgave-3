"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var beer_1 = require('./beer');
var BeerDetailComponent = (function () {
    function BeerDetailComponent() {
    }
    __decorate([
        core_1.Input(), 
        __metadata('design:type', beer_1.Beer)
    ], BeerDetailComponent.prototype, "beer", void 0);
    BeerDetailComponent = __decorate([
        core_1.Component({
            selector: 'my-beer-detail',
            template: "\n\t<div *ngIf=\"beer\">\n     <h2>Details about {{beer.name}}</h2>\n     <div>name: {{beer.name}}</div>\n     <div>style: {{beer.style_name}}</div>\n     <div>brewery: {{beer.brewery_name}}</div>\n     <div>alcohol level: {{beer.abv}}%</div>\n     <div>:description {{beer.description}}</div>\n   \t</div>\n\n\n  "
        }), 
        __metadata('design:paramtypes', [])
    ], BeerDetailComponent);
    return BeerDetailComponent;
}());
exports.BeerDetailComponent = BeerDetailComponent;
//# sourceMappingURL=beer-detail.component.js.map