import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }	from '@angular/forms';
import { AppComponent }		from './app.component';
import { BeerDetailComponent } from './beer-detail.component';
import { HttpModule } from "@angular/http";


@NgModule({
  imports:      [ 
  BrowserModule ,
  FormsModule,
  HttpModule
  ],
  declarations: [ 
  AppComponent,
  BeerDetailComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }


