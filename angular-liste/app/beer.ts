export class Beer {
	beer_id: number;
	name: string;
	style_id: number;
  	style_name: string;
  	abv: number;
	brewery_id : string;
	brewery_name : number;
	description : string;

	constructor(beer_id, beer_name, style_id, style_name, abv, brewery_id, brewery_name, description){
		this.beer_id = beer_id;
		this.name = beer_name;
		this.style_id = style_id;
		this.style_name = style_name;
		this.abv = abv;
		this.brewery_id = brewery_id;
		this.brewery_name = brewery_name;
		this.description = description;
	}
}
