import { Component, Input } from '@angular/core';
import { Beer } from './beer';

@Component({
  selector: 'my-beer-detail',
  template: `
	<div *ngIf="beer">
     <h2>Details about {{beer.name}}</h2>
     <div>name: {{beer.name}}</div>
     <div>style: {{beer.style_name}}</div>
     <div>brewery: {{beer.brewery_name}}</div>
     <div>alcohol level: {{beer.abv}}%</div>
     <div>:description {{beer.description}}</div>
   	</div>


  `
})
export class BeerDetailComponent {
	@Input()
	beer: Beer;
}
