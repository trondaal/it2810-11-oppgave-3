"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var beer_service_1 = require('./beer.service');
var Rx_1 = require('rxjs/Rx');
var AppComponent = (function () {
    function AppComponent(beerService) {
        this.beerService = beerService;
        this.title = 'Beer with us';
        this.subject = new Rx_1.Subject();
    }
    AppComponent.prototype.getBeers = function (str) {
        var _this = this;
        this.beerService.search_beer(str).subscribe(function (beers) { return _this.beers = beers; });
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getBeers("");
        this.subject
            .debounceTime(300)
            .distinctUntilChanged()
            .switchMap(function (str, i) {
            return _this.beerService.search_beer(str);
        })
            .subscribe(function (beers) { return _this.beers = beers; });
        this.subject.debounceTime(300).subscribe(function (v) { return console.log(v); });
    };
    AppComponent.prototype.onSelect = function (beer) {
        this.selectedBeer = beer;
    };
    AppComponent.prototype.search = function (event) {
        this.subject.next(event.target["value"]);
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'my-app',
            template: "\n \t<h2>{{title}}</h2>\n\n  <input type=\"text\" name=\"search\" (keyup)=\"search($event)\"/>\n\n \t<table class=\"beers\">\n \t\t<tr>\n \t\t\t<th> Beer-name </th>\n      <th> Beer-style </th>\n      <th> Brewery </th>\n \t\t</tr>\n \t\t<tr *ngFor=\"let beer of beers\" \n     [class.selected]=\"selectedBeer && (beer.beer_id === selectedBeer.beer_id)\"\n     (click)=\"onSelect(beer)\">\n \t\t  <td class=\"beers\"> {{beer.name}} </td>\n      <td class=\"beers\"> {{beer.style_name}} </td>\n      <td class=\"beers\"> {{beer.brewery_name}} </td>\n \t\t</tr>\n \t</table>\n\n  <my-beer-detail [beer]=\"selectedBeer\"></my-beer-detail>\n\n   ",
            styles: ["\n  table, th , td  {\n    border: 1px solid grey;\n    border-collapse: collapse;\n    padding: 5px;\n  }\n  table tr:nth-child(odd) {\n    background-color: #f1f1f1;\n  }\n  table tr:nth-child(even) {\n    background-color: #ffffff;\n  }\n  table tr:hover {\n    background-color: yellow;\n  }\n  table tr.selected {\n    background-color: orange;\n  }\n  "],
            providers: [beer_service_1.BeerService]
        }), 
        __metadata('design:paramtypes', [beer_service_1.BeerService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map