import { Component, OnInit } from '@angular/core';
import { Beer } from './beer';
import { BeerService } from './beer.service';
import { Observable, Subject } from 'rxjs/Rx';


@Component({
  selector: 'my-app',
  template: `
 	<h2>{{title}}</h2>

  <input type="text" name="search" (keyup)="search($event)"/>

 	<table class="beers">
 		<tr>
 			<th> Beer-name </th>
      <th> Beer-style </th>
      <th> Brewery </th>
 		</tr>
 		<tr *ngFor="let beer of beers" 
     [class.selected]="selectedBeer && (beer.beer_id === selectedBeer.beer_id)"
     (click)="onSelect(beer)">
 		  <td class="beers"> {{beer.name}} </td>
      <td class="beers"> {{beer.style_name}} </td>
      <td class="beers"> {{beer.brewery_name}} </td>
 		</tr>
 	</table>

  <my-beer-detail [beer]="selectedBeer"></my-beer-detail>

   `,
  styles: [`
  table, th , td  {
    border: 1px solid grey;
    border-collapse: collapse;
    padding: 5px;
  }
  table tr:nth-child(odd) {
    background-color: #f1f1f1;
  }
  table tr:nth-child(even) {
    background-color: #ffffff;
  }
  table tr:hover {
    background-color: yellow;
  }
  table tr.selected {
    background-color: orange;
  }
  `],
  providers: [BeerService]

})

export class AppComponent implements OnInit { 
	title = 'Beer with us';
	beers: Beer[];
  selectedBeer: Beer;
  subject : Subject<string> = new Subject<string>();


  constructor(private beerService: BeerService) { }

  getBeers(str : string): void {
    this.beerService.search_beer(str).subscribe(beers => this.beers = beers);
  }


  ngOnInit(): void {
    this.getBeers("");
    this.subject
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap((str : string, i : number) => {
        return this.beerService.search_beer(str);
      })
      .subscribe(beers => this.beers = beers);

    this.subject.debounceTime(300).subscribe(v => console.log(v))
  }

  onSelect(beer: Beer): void{
    this.selectedBeer = beer;
  }

  search(event: KeyboardEvent){
    this.subject.next(event.target["value"]);
  }


}
